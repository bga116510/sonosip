﻿namespace SonoSip.Tools.SoftPhone
{
    using pjsua2;

    class CustomAccount : Account
    {
        SoftPhone ancestor;

        public CustomAccount(SoftPhone softPhone)
        {
            ancestor = softPhone;
        }

        public override void onRegState(OnRegStateParam param)
        {
            ancestor.OnRegStateUpdated(param);
        }

        public override void onIncomingCall(OnIncomingCallParam param)
        {
            ancestor.OnIncomingCall(param);
        }

    }
}
