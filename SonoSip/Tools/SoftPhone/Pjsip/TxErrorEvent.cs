/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 2.0.12
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

namespace pjsua2 {

using System;
using System.Runtime.InteropServices;

public class TxErrorEvent : IDisposable {
  private HandleRef swigCPtr;
  protected bool swigCMemOwn;

  internal TxErrorEvent(IntPtr cPtr, bool cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = new HandleRef(this, cPtr);
  }

  internal static HandleRef getCPtr(TxErrorEvent obj) {
    return (obj == null) ? new HandleRef(null, IntPtr.Zero) : obj.swigCPtr;
  }

  ~TxErrorEvent() {
    Dispose();
  }

  public virtual void Dispose() {
    lock(this) {
      if (swigCPtr.Handle != IntPtr.Zero) {
        if (swigCMemOwn) {
          swigCMemOwn = false;
          pjsua2PINVOKE.delete_TxErrorEvent(swigCPtr);
        }
        swigCPtr = new HandleRef(null, IntPtr.Zero);
      }
      GC.SuppressFinalize(this);
    }
  }

  public SipTxData tdata {
    set {
      pjsua2PINVOKE.TxErrorEvent_tdata_set(swigCPtr, SipTxData.getCPtr(value));
    } 
    get {
      IntPtr cPtr = pjsua2PINVOKE.TxErrorEvent_tdata_get(swigCPtr);
      SipTxData ret = (cPtr == IntPtr.Zero) ? null : new SipTxData(cPtr, false);
      return ret;
    } 
  }

  public SipTransaction tsx {
    set {
      pjsua2PINVOKE.TxErrorEvent_tsx_set(swigCPtr, SipTransaction.getCPtr(value));
    } 
    get {
      IntPtr cPtr = pjsua2PINVOKE.TxErrorEvent_tsx_get(swigCPtr);
      SipTransaction ret = (cPtr == IntPtr.Zero) ? null : new SipTransaction(cPtr, false);
      return ret;
    } 
  }

  public TxErrorEvent() : this(pjsua2PINVOKE.new_TxErrorEvent(), true) {
  }

}

}
