namespace SonoSip.Tools.RemoteControl.NancyModule
{
    using Nancy;
    using Nancy.Authentication.Forms;
    using Nancy.Extensions;
    using System;
    using System.Dynamic;

    public class LoginModule : NancyModule
    {
        public LoginModule()
        {
            Get["/login"] = x =>
                {
                    dynamic model = new ExpandoObject();
                    model.Errored = this.Request.Query.error.HasValue;
                    model.Users = UserDatabase.getUsernames();

                    return View["login", model];
                };

            Post["/login"] = x => {
                var userGuid = UserDatabase.ValidateUser((string)this.Request.Form.Username, (string)this.Request.Form.Password);

                if (userGuid == null)
                {
                    return this.Context.GetRedirect("~/login?error=true&username=" + (string)this.Request.Form.Username);
                }

                DateTime? expiry = null;

                return this.Login(userGuid.Value, expiry);
            };

            Get["/logout"] = x => {
                return this.Logout("~/");
            };
        }
    }
}