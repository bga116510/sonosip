﻿namespace SonoSip.Tools.RemoteControl.NancyModule
{
    using Nancy;
    using Nancy.Security;
    using SonoSip.Tools.SoftPhone;

    public class MainModule : NancyModule
    {
        public MainModule()
        {
            this.RequiresAuthentication();

            Get["/"] = parameters => View["Index"];


            Get["/callList"] = parameters =>
            {
                var callList = SoftPhone.Instance.callList;
                string callListSerialized = Newtonsoft.Json.JsonConvert.SerializeObject(callList);
                return Response.AsJson<string>(callListSerialized);
            };

        }
    }
}
