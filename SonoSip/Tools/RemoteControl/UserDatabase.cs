namespace SonoSip.Tools.RemoteControl
{
    using Nancy;
    using Nancy.Authentication.Forms;
    using Nancy.Security;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class UserDatabase : IUserMapper
    {
        private static List<Tuple<string, string, Guid, IEnumerable<string>>> users = new List<Tuple<string, string, Guid, IEnumerable<string>>>();

        public IUserIdentity GetUserFromIdentifier(Guid identifier, NancyContext context)
        {
            var userRecord = users.Where(u => u.Item3 == identifier).FirstOrDefault();

            return userRecord == null
                       ? null
                       : new UserIdentity { UserName = userRecord.Item1, Claims = userRecord.Item4 };
        }

        public static Guid? ValidateUser(string username, string password)
        {
            var userRecord = users.Where(u => u.Item1 == username && u.Item2 == password).FirstOrDefault();

            if (userRecord == null)
            {
                return null;
            }

            return userRecord.Item3;
        }

        public static void AddUser(string username, string password, IEnumerable<string> claims)
        {
            var userRecord = users.Where(u => u.Item1 == username).FirstOrDefault();

            if (userRecord == null)
            {
                users.Add(new Tuple<string, string, Guid, IEnumerable<string>>(username, password, Guid.NewGuid(), claims));
            }
        }

        public static void ClearUserList()
        {
            users.Clear();
        }

        public static void RemoveUser(string username)
        {
            var userRecord = users.Where(u => u.Item1 == username).FirstOrDefault();

            if (userRecord != null)
            {
                users.Remove(userRecord);
            }
        }

        public static IEnumerable<string> getUsernames()
        {
            List<string> usernames = new List<string>();

            foreach (Tuple<string, string, Guid, IEnumerable<string>> user in users)
            {
                usernames.Add(user.Item1);
            }

            return usernames;
        }
    }
}