﻿
var sonosipApp = angular.module('sonosipApp', []);

sonosipApp.filter('callstate', function() {
    return function(input) {
		input = input || '';
		var out = "";

		switch (input) 
		{ 
		case 5: 
			out = "Attend le début de la retransmission";
			break;
		case 6: 
			out = "Ecoute le programme";
			break;
		case 7: 
			out = "Demande à participer";
			break;
		case 8: 
			out = "Micro activé";
			break;
		case 9: 
			out = "Ecoute le message de déconnexion";
			break; 
		default: 
			out = "Etat inconnu";
			break; 
		}

		return out;
	};
});

sonosipApp.filter('callstateicon', function() {
    return function(input) {
		input = input || '';
		var out = "";

		switch (input) 
		{ 
		case 5: 
			out = "check-circle";
			break;
		case 6: 
			out = "check-circle";
			break;
		case 7: 
			out = "exclamation-circle";
			break;
		case 8: 
			out = "microphone";
			break;
		case 9: 
			out = "times-circle";
			break; 
		default: 
			out = "question-circle";
			break; 
		}

		return out;
	};
});

sonosipApp.controller('CallListCtrl', function ($scope, $http, $timeout) {

    $scope.calls = "";

	var poll = function () {
	    $timeout(function () {
	        $http({ method: 'GET', url: '/callList' }).
                success(function (data, status, headers, config) {
                    $scope.calls = angular.fromJson(angular.fromJson(data));
                    poll();
                }).
                error(function (data, status, headers, config) {
                    alert("Oups ! Il semble y avoir un problème avec le serveur SonoSip")
                });
	    }, 1000);
	};
	poll();
	
	$scope.connectMicrophone = function(callId) {
		$http.get(rootApiUrl + 'connectMicrophone/' + callId);
	}
	$scope.disconnectMicrophone = function(callId) {
		$http.get(rootApiUrl + 'disconnectMicrophone/' + callId);
	}
	$scope.cancelSpeakRequest = function(callId) {
		$http.get(rootApiUrl + 'cancelSpeakRequest/' + callId);
	}

});


